# Prometheus-node-exporter

This role will provision prometheus-node-exporter

# Variables

Prometheus node exporter packages

    prometheus_node_exporter_pkgs

# Defaults

Prometheus node exporter port

    prometheus_node_exporter_port

Prometheus shell user and group

    prometheus_node_exporter_shell_user
    prometheus_node_exporter_shell_group
