# Ansible automation for Grafana and Prometheus

Simple Ansible automation to provision monitoring server and configure agents on
servers we need to monitor.

# Description

In short: Grafana draws, Prometheus collects. Both of them reside on one server.

We will call it monitoring server in this example.

Grafana uses underlying relational database. We have chosen PostgreSQL for our
use-case.

Grafana conects on web interface of Prometheus server and takes metrics.

Prometheus has its own timeseries integrated database.

Prometheus collects metrics from monitored servers by contacting exporters.

Exporter is a web endpoint which provides all metrics and Prometheus connects
to it at given intervals (5sec for example) and takes metrics.

One monitored server can have several exporters, each of them on its own port.

# Network ports

Default network ports (also used in this setup):

- Grafana server: 3000
- Prometheus server: 9090
- Prometheus node exporter: 9100
- Prometheus postgres exporter: 9187
- Prometheus mysql exporter: 9104

# Supports

These roles are made for Debian 10.  Some adjustmets are needed to supports
other Linux flavors.

# Roles

Each role has its respectable README file.

- grafana (Grafana server)
- prometheus-server (Prometheus server)
- prometheus-node-exporter (node exporter)
- prometheus-postgres-exporter (postgres exporter)
- prometheus-mysqld-exporter (mysql exporter)
