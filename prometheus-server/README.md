# Prometheus-server

This role will provision prometheus server and configure jobs
and clients based on the dictionary provided.

# Variables

Prometheus packages

    prometheus_server_pkgs

# Defaults

Prometheus user and group

    prometheus_os_user
    prometheus_os_group

Prometheus web interface port

    prometheus_port

Prometheus node exporter port

    prometheus_node_exporter_port

Prometheus postgres exporter port

    prometheus_postgres_exporter_port

Prometheus mysqld exporter port

    prometheus_mysqld_exporter_port

Prometheus scrape interval

    prometheus_scrape_interval

Prometheus evaluation interval

    prometheus_evaluation_interval

Prometheus base data directory

    prometheus_datadir

Prometheus metrics path

    prometheus_metrics_path

Prometheus managed nodes

    prometheus_nodes

Example:

```
prometheus_nodes:
  - job_name: "Proxy servers"
    nodes:
      - proxy-dev.yggdrasil.local:{{ prometheus_node_exporter_port }}
      - proxy.yggdrasil.local:{{ prometheus_node_exporter_port }}
  - job_name: "PostgreSQL DB"
    nodes:
      - pgsqlm.yggdrasil.local:{{ prometheus_postgres_exporter_port }}
      - pgsqlm.yggdrasil.local:{{ prometheus_node_exporter_port }}
  - job_name: "MySQL DB"
    nodes:
      - web01.yggdrasil.local:{{ prometheus_mysqld_exporter_port }}
      - web01.yggdrasil.local:{{ prometheus_node_exporter_port }}
```
