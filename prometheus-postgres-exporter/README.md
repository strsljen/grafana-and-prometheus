# Prometheus-postgres-exporter

This role will provision prometheus-postgres-exporter.

Prometheus-postgres-exporter is intended to run on server where
monitored dbserver is.

Therefore, it is by definition configured to connect and monitor
dbserver on localhost.

# Variables

Prometheus node exporter packages

    prometheus_node_exporter_pkgs

# Defaults

Prometheus node exporter port

    prometheus_node_exporter_port

Prometheus shell user and group

    prometheus_node_exporter_shell_user
    prometheus_node_exporter_shell_group

Prometheus db user and password

    prometheus_postgres_exporter_dbuser
    prometheus_postgres_exporter_dbpassword

PostgreSQL shell user

    prometheus_postgres_exporter_postgres_shell_user
