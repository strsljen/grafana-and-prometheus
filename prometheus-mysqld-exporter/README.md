# Prometheus-mysqld-exporter

This role will provision prometheus-mysqld-exporter.

Prometheus-mysqld-exporter is intended to run on server where
monitored dbserver is.

Therefore, it is by definition configured to connect and monitor
dbserver on localhost.

# Variables

Prometheus-mysqld-exporter packages

    prometheus_mysqld_exporter_pkgs

# Defaults

Prometheus mysqld exporter port

    prometheus_mysqld_exporter_port

Prometheus shell user and group

    prometheus_mysqld_exporter_shell_user
    prometheus_mysqld_exporter_shell_group

Prometheus db user and password


    prometheus_mysqld_exporter_dbuser
    prometheus_mysqld_exporter_dbpassword
