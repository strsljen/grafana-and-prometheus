# Grafana

This role will provision Grafana.

It will also install underlying PostgreSQL database and do some basic Grafana
configuration:
  - set admin password
  - add prometheus data source

Grafana will be installed from external rerpository (also added in by this
role. We will install Grafana Enterprise edition.

# Variables

Grafana repository and signing key

    grafana_repository
    grafana_key

PostgreSQL server packages

    postgresql_pkgs

Grafana server packages

    grafana_pkgs


# Defaults

PostgreSQL shell user and group

    grafana_postgresql_shell_user
    grafana_postgresql_shell_group

Grafana shell user and group

    grafana_dbserver
    grafana_dbport
    grafana_dbserver_type

Grafana db user and password
(Password has te be changed and vaulted!)

    grafana_db_user
    grafana_db_password

Grafana admin user and password
(Password has te be changed and vaulted!)

    grafana_admin_user
    grafana_admin_password

Grafana database name

    grafana_dbname

PostgreSQL users for Grafana

    grafana_postgresql_users

PostgreSQL databases for Grafana

    grafana_postgresql_databases

Grafana data directory

    grafana_datadir

Grafana protocol

    grafana_protocol

Grafana port

    grafana_port

Grafana domain

    grafana_domain

Prometheus server web interface port

    grafana_prometheus_port


